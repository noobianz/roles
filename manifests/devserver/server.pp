# == Class: role::devserver::server
#
# === Parameters
#
# None
#
# === Variables
#
# None
#
# === Examples
#
#  include role::devserver::server
#
# === Authors
#
# Rodney Brown <rodney@punchtech.com>
#
# === Copyright
#
# Copyright 2015 Rodney Brown
#
class roles::devserver::server {
  include profiles::base
}

